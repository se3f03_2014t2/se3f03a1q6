#!/bin/bash

if [ -n "$1" ]; then
	cd $1
fi

for i in *;do FOLDER=$(find $i -name findexec* -printf '%h\n'|head -n1); [ -z $FOLDER ] || { docker run --rm=true -v `pwd`/$FOLDER:/under_test:ro mjdsys/se3f03a1q6 qchecker /under_test >$i/A1q5.out 2>&1;echo $i,$?; }; done
