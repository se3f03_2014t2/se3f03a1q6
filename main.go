package main 

import (
	. "gitlab.com/se3f03_2014t2/se3f03base"
	"fmt"
	"os"
)

func runTest(points int, scriptName, ans string, searches ...string) {
	fmt.Println("Trying to find: ", searches)
	output := ExecuteScript(append([]string{scriptName}, searches...)...)
	if CompareNewlineStrings(output, ans) {
		fmt.Println("Match found!")
		GM.Lock()
		Grade += points
		GM.Unlock()
	} else {
		fmt.Println("Wrong output given.  Wanted:\n", ans, "\nGot:\n", output)
	}
}

func main() {
	folder := os.Args[1]
	folderfd, err := os.Open(folder)
	if err != nil {
		fmt.Println("Failed to Open dir: ", err)
		os.Exit(-1)
	}
	files, err := folderfd.Readdirnames(-1)
	if err != nil {
		fmt.Println("Failed to Readdirnames: ", err)
		os.Exit(-1)
	}
	
	scriptName := folder + "/" + FindScript("findexec", files)
	if scriptName == "/under_test/" {
		fmt.Println("Failed to find findexec, only found files (should never happen): ", files)
		os.Exit(-2)
	} else {
		fmt.Println("Running: ", scriptName)
	}
	
	runTest(4, scriptName, "/bin/bash\n", "bash")
	
	runTest(2, scriptName, "/usr/bin/faked-tcp\n/usr/bin/fakeroot-tcp\n/usr/bin/fakeroot-sysv\n/usr/bin/faked-sysv\n", "fake*-*")
	
	runTest(2, scriptName, "/usr/bin/touch\n/bin/touch\n", "touch")
	
	runTest(2, scriptName, "/usr/bin/top\n/bin/sync\n", "top", "sync")
	
	os.Exit(Grade)
}
